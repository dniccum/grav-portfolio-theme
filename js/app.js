'use strict';

/**
 * @ngdoc overview
 * @name portfolioApp
 * @description
 * # portfolioApp
 *
 * Main module of the application.
 */
angular
    .module('portfolioApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngTouch'
    ])
    /*
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $urlRouterProvider
            .when('', '/');

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl',
                data: {
                    pageTitle: 'Innovation through Creativity and Experience',
                    bodyClass: 'home',
                    pageDescription: 'A Kansas City-based web and application developer with a passion for implementing new ideas and technology.'
                }
            })
            .state('about', {
                url: '/about',
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                data: {
                    pageTitle: 'About Me',
                    bodyClass: 'about',
                    pageDescription: 'My name is Doug Niccum. I am a quality-driven web and mobile application developer.'
                }
            })
            .state('portfolio', {
                url: '/portfolio',
                templateUrl: 'views/portfolio.html',
                controller: 'PortfolioCtrl',
                data: {
                    pageTitle: 'My Work and Portfolio',
                    bodyClass: 'portfolio',
                    pageDescription: 'My work is diverse because my skillset is diverse. I enjoy challenges.'
                }
            })
            .state('what-should-i-tip', {
                url: '/portfolio/what-should-i-tip',
                templateUrl: 'views/portfolio-what-should-i-tip.html',
                controller: 'PortfolioCtrl',
                data: {
                    pageTitle: 'What Should I Tip | Portfolio',
                    bodyClass: 'portfolio'
                }
            })
            .state('yorbit', {
                url: '/portfolio/yorbit',
                templateUrl: 'views/portfolio-yorbit.html',
                controller: 'PortfolioCtrl',
                data: {
                    pageTitle: 'Yorbit | Portfolio',
                    bodyClass: 'portfolio'
                }
            })
            .state('shipreq', {
                url: '/portfolio/shipreq',
                templateUrl: 'views/portfolio-shipreq.html',
                controller: 'PortfolioCtrl',
                data: {
                    pageTitle: 'ShipReq | Portfolio',
                    bodyClass: 'portfolio'
                }
            })
            // .state('epr-insight-center', {
            //     url: '/portfolio/epr-insight-center',
            //     templateUrl: 'views/portfolio-epr.html',
            //     controller: 'PortfolioCtrl',
            //     data: {
            //         pageTitle: 'EPR Insight Center | Portfolio',
            //         bodyClass: 'portfolio'
            //     }
            // })
            .state('chemistry-marketing-website', {
                url: '/portfolio/chemistry-marketing-website',
                templateUrl: 'views/portfolio-chemistry.html',
                controller: 'PortfolioCtrl',
                data: {
                    pageTitle: 'Chemistry Marketing Website | Portfolio',
                    bodyClass: 'portfolio'
                }
            })
            .state('lockton', {
                url: '/portfolio/lockton-corporate-website',
                templateUrl: 'views/portfolio-lockton.html',
                controller: 'PortfolioCtrl',
                data: {
                    pageTitle: 'Lockton Corporate Website | Portfolio',
                    bodyClass: 'portfolio'
                }
            })
            .state('cosmix', {
                url: '/portfolio/cosmix',
                templateUrl: 'views/portfolio-cosmix.html',
                controller: 'PortfolioCtrl',
                data: {
                    pageTitle: 'Cosmix | Portfolio',
                    bodyClass: 'portfolio'
                }
            })
            .state('quiq-math-privacy-policy', {
              url: '/quiq-math-privacy-policy',
              templateUrl: 'views/quiq-math-privacy-policy.html',
              data: {
                  pageTitle: 'Quiq Math Privacy Policy',
                  bodyClass: 'about'
              }
            });
        }])

    // ADDS PAGE TITLE AND BODY CLASS
    .run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }])
    */
    // GLOBAL NAV CONTROLLER
    .controller('navCtrl', ['$scope', '$anchorScroll', '$location', function ($scope, $anchorScroll, $location) {
        $scope.navVisible = false;
        $scope.toggleNav = function() {
            if ($scope.navVisible) {
                $scope.navVisible = false;
            } else {
                $scope.navVisible = true;
            }
        };
        $scope.navGo = function() {
            $location.hash('header');
            $anchorScroll();
            $scope.navVisible = false;
        };
    }]);
