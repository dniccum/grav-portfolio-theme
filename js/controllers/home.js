'use strict';

/**
 * @ngdoc function
 * @name portfolioApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the portfolioApp
 */
angular.module('portfolioApp')
	.controller('HomeCtrl', function ($scope) {
		$scope.scrollToWork = function() {
			console.log("what");
			angular.element('html,body').animate({
				scrollTop: (jQuery('section.work').offset().top)
			});
		};
	});
